package  java_rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FileTransferClient extends Remote {
    void sendFile(String fileName, long fileSize, byte[] fileData) throws RemoteException;
}
