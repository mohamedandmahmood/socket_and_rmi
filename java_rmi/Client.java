package java_rmi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Client {

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.getRegistry("127.0.0.1", 1099);
            FileTransferServer fileTransferServer = (FileTransferServer) registry.lookup("FileTransferServer");


            long latency =0;
            long fileSize =0;
            long throughput =0;

            for (int i = 0; i < args.length; i++) {
                String filePath = args[i];  // Set the file path here
                Map<String,Long> summery = sendFileToServer(fileTransferServer, filePath);
                latency = latency+summery.get("latency");
                fileSize = fileSize+summery.get("fileSize");
                throughput = throughput+summery.get("throughput");
            }

            System.out.println("Reports of "+ args.length +" Files");
            System.out.println("Total MB: "+ fileSize + " MB");
            System.out.println("Average latency: "+ latency/args.length + " MS");
            System.out.println("Average throughput: "+ throughput/args.length + " Megabytes per second");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<String,Long> sendFileToServer(FileTransferServer fileTransferServer, String filePath) {


    Map<String,Long>  summery = new HashMap<>();
        long startTime = System.currentTimeMillis();

        try (FileInputStream fis = new FileInputStream(filePath)) {
            File file = new File(filePath);

            byte[] fileData = new byte[(int) file.length()];
            fis.read(fileData);

            long fileSize = file.length();
            long fileSizeInKB = fileSize / 1000;
            String fileName = file.getName();

            // Send file details and content to the server
            String md5Hash = fileTransferServer.transferFile(fileName, fileSize, fileData);
            long endTime = System.currentTimeMillis();
            long processingTime = endTime - startTime;
            float latency = (float) (endTime - startTime) /1000;

            long throughput = (long) (fileSize/latency)/1000/1000;

            System.out.println("file name: "+ fileName  );
            System.out.println("file size: " + fileSizeInKB/1000  + " MB");
            System.out.println("latency: " + processingTime +" MS" );
            System.out.println("throughput: "+ throughput+" MegaByte per second" );
            System.out.println("MD5 Hash of the received file: " + md5Hash );
            System.out.println("____________________________________________");

            summery.put("fileSize",fileSizeInKB/1000);
            summery.put("latency",processingTime);
            summery.put("throughput",throughput);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return summery;
    }
}

