package java_rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public static void main(String[] args) {
        try {

            FileTransferServer fileTransferServer = new FileTransferServerImpl();

            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("FileTransferServer", fileTransferServer);

            System.out.println("FileTransferServer is ready to accept requests.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
