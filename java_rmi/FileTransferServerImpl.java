package java_rmi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.MessageDigest;

import java.security.NoSuchAlgorithmException;
public class FileTransferServerImpl extends UnicastRemoteObject implements FileTransferServer {

    protected FileTransferServerImpl() throws RemoteException {
        super();
    }

    @Override
    public String transferFile(String fileName, long fileSize, byte[] fileData) throws RemoteException {
        try {
            // Save the received file
            FileOutputStream fos = new FileOutputStream("received_" + fileName);
            fos.write(fileData);
            fos.close();

            // Calculate and return MD5 hash
            String md5Hash = calculateMD5("received_" + fileName);
            return md5Hash;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String calculateMD5(String filePath) {
        // Implement the MD5 calculation logic here
        try (FileInputStream fis = new FileInputStream(filePath)) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[8192];
            int bytesRead;

            while ((bytesRead = fis.read(buffer)) != -1) {
                md.update(buffer, 0, bytesRead);
            }

            byte[] md5Hash = md.digest();
            StringBuilder md5HashString = new StringBuilder();

            for (byte b : md5Hash) {
                md5HashString.append(String.format("%02x", b));
            }

            return md5HashString.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
