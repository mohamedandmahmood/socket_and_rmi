package java_rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class FileTransferClientImpl extends UnicastRemoteObject implements FileTransferClient {

    protected FileTransferClientImpl() throws RemoteException {
        super();
    }

    @Override
    public void sendFile(String fileName, long fileSize, byte[] fileData) throws RemoteException {
        // Implement the logic to send the file to the server

    }
}
