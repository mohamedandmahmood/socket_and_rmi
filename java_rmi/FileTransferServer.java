package java_rmi;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FileTransferServer extends Remote {
    String transferFile(String fileName, long fileSize, byte[] fileData) throws RemoteException;
}
