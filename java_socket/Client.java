package java_socket;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class Client {

    public static void main(String[] args) {

        System.out.println("Testing network transfer using Java socket");
        String filePath= "";
        if(args.length==0){
            System.out.println("Please run java java_socket.Client <filename.ext> ");
            System.exit(0);
        }

        String serverAddress = "127.0.0.1";
        int port = 12345;

        long latency =0;
        long fileSize =0;
        long throughput =0;

        try {
            Socket socket = null;
            System.out.println("Connected to server");

            for (int i = 0; i < args.length; i++) {
                socket =  new Socket(serverAddress, port);
              Map<String,Long> summery= sendFile(socket, args[i]);
              latency = latency+summery.get("latency");
              fileSize = fileSize+summery.get("fileSize");
              throughput = throughput+summery.get("throughput");
            }


            System.out.println("Connection closed");

            System.out.println("Reports of "+ args.length +" Files");
            System.out.println("Total MB: "+ fileSize + " MB");
            System.out.println("Average latency: "+ latency/args.length + " MS");
            System.out.println("Average throughput: "+ throughput/args.length + " Megabytes per second");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Map<String,Long> sendFile(Socket socket, String filePath) {
        Map<String, Long> summery =new HashMap<>();
        try {

            DataInputStream dis = new DataInputStream(new FileInputStream(filePath));
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

            // Send file details
            dos.writeUTF(new File(filePath).getName());
            dos.writeLong(new File(filePath).length());

            // Send file content
            byte[] buffer = new byte[4096];
            int bytesRead;

            long startTime = System.currentTimeMillis();
            long fileSize = new File(filePath).length();
            long fileSizeInKB = fileSize / 1024;


            System.out.println("Sending file: " + filePath);
            while ((bytesRead = dis.read(buffer)) != -1) {
                dos.write(buffer, 0, bytesRead);
            }

            dos.flush();

            long endTime = System.currentTimeMillis();
            long processingTime = endTime - startTime;
            float latency = (float) (endTime - startTime) /1000;
            long throughput = (long) (fileSize/latency)/1000/1000;

            // Receive and print MD5 hash
            DataInputStream response = new DataInputStream(socket.getInputStream());
            String md5Hash = response.readUTF();

            System.out.println("file name: "+ new File(filePath).getName()  );
            System.out.println("file size: " + fileSizeInKB/1000  + " MB");
            System.out.println("latency: " + processingTime +" MS" );
            System.out.println("throughput: "+ throughput+" MegaByte per second" );
            System.out.println("MD5 Hash of the received file: " + md5Hash );
            System.out.println("____________________________________________");

            summery.put("fileSize",fileSizeInKB/1000);
            summery.put("latency",processingTime);
            summery.put("throughput",throughput);

            dis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return summery;
    }
}
