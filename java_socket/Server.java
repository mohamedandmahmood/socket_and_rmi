package java_socket;

import java.io.*;
import java.net.*;
import java.security.MessageDigest;
import java.util.Date;

public class Server {

    public static void main(String[] args) {
        int port = 12345;

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("java_socket.Server listening on port " + port);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("java_socket.Client connected: " + clientSocket.getInetAddress());

                handleClient(clientSocket);

                clientSocket.close();
                System.out.println("java_socket.Client disconnected");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void handleClient(Socket clientSocket) {
        try {
            long startTime = System.currentTimeMillis();

            DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
            DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream());

            // Receive file
            String fileName = dis.readUTF();
            long fileSize = dis.readLong();

            FileOutputStream fos = new FileOutputStream("received_" + fileName);
            byte[] buffer = new byte[4096];
            int bytesRead;

            System.out.println("File begin Receiving on" + new Date().toLocaleString());
            System.out.println("Receiving file: " + fileName);
            while (fileSize > 0 && (bytesRead = dis.read(buffer, 0, (int) Math.min(buffer.length, fileSize))) != -1) {
                fos.write(buffer, 0, bytesRead);
                fileSize -= bytesRead;
            }
            fos.close();
            System.out.println("File received successfully");
            System.out.println("File receiving done on " + new Date().toLocaleString());

            // Calculate and send MD5 hash
            String md5Hash = calculateMD5("received_" + fileName);

            long endTime = System.currentTimeMillis();
            long processingTime = endTime - startTime;
            float latency = (float) (endTime - startTime) /1000;
            long throughput = (long) (fileSize/latency)/1024/1024;
            dos.writeUTF(md5Hash );

            dos.flush();
            System.out.println("Processing time: " + processingTime + " milliseconds");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String calculateMD5(String filePath) throws IOException {
        try (FileInputStream fis = new FileInputStream(filePath)) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[8192];
            int bytesRead;

            while ((bytesRead = fis.read(buffer)) != -1) {
                md.update(buffer, 0, bytesRead);
            }

            byte[] md5Hash = md.digest();
            StringBuilder md5HashString = new StringBuilder();

            for (byte b : md5Hash) {
                md5HashString.append(String.format("%02x", b));
            }

            return md5HashString.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
